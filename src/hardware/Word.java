package hardware;

public interface Word extends Operator{
	
	public long getValue();
	
	public Word add(Word w2);
	
	public Word mul(Word w2);

}
