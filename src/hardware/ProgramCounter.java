package hardware;

public class ProgramCounter {
	private int current;
	
	public ProgramCounter(){
		current = 0;
	}
	
	public void update(){
		current++;
	}
	
	public void setCounter(int newCurrent){
		current = newCurrent;
	}
	
	public int getCurrent(){
		return current;
	}
	
	public void halt(){
		setCounter(-1);
	}
	
	public boolean running(){
		return current > -1;
	}
}

