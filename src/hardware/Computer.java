package hardware;

import software.Program;

public class Computer {
	private ProgramCounter pc;
	private Program program;
	private Memory memory;
	
	public Computer(Memory memory){
		pc = new ProgramCounter();
		this.memory = memory;
	}
	
	public void load(Program program){
		this.program = program;
	}
	
	public void run(){
		while(pc.running()){
			program.get(pc.getCurrent()).execute(pc, memory);
		}
	}
}
