package hardware;

import java.util.HashMap;

public class LongMemory implements Memory {
	private HashMap<Integer, LongWord> memory;
	private int size;
	
	public LongMemory(int size) {
		memory = new HashMap<Integer, LongWord>();
		this.size = size;
	}

	@Override
	public Word get(Address adr) {
		return memory.get(adr.getLocation());
	}

	public void set(Word lw, Address adr) {
		if(adr.getLocation() < size && adr.getLocation() > -1){
			memory.put(adr.getLocation(), new LongWord(lw.getValue()));
		} else {
			System.out.println("Trying to set out of bounds");
		}
	}
}
