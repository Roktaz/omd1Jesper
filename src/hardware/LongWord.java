package hardware;

public class LongWord implements Word {
	private long value;
	
	public LongWord(long value){
		this.value = value;
	}
	
	public long getValue() {
		return value;
	}

	@Override
	public Word add(Word w2) {
		return new LongWord(value + w2.getValue());
	}

	@Override
	public Word mul(Word w2) {
		return new LongWord(value * w2.getValue());
	}

	@Override
	public Word getWord(Memory memory) {
		return this;
	}
	
	public String toString(){
		Long w = value;
		return w.toString();
	}

}
