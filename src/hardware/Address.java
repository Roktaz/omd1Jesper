package hardware;

public class Address implements Operator {
	private int location;
	
	public Address(int location) {
		this.location = location;
	}
	
	public int getLocation() {
		return location;
	}

	@Override
	public Word getWord(Memory memory) {
		return memory.get(this);
	}
	
	public String toString(){
		return "[" + location + "]";
	}

}
