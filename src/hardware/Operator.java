package hardware;

public interface Operator {
	
	public Word getWord(Memory memory);
	
	public String toString();

}
