package hardware;

public interface Memory {
	
	public Word get(Address adr);
	
	public void set(Word word, Address adr);

}
