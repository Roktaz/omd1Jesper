package software;

import hardware.Memory;
import hardware.ProgramCounter;

public interface Instruction {
	
	public void execute(ProgramCounter pc, Memory memory);
	
	public String toString();
}
