package software;

import hardware.Address;
import hardware.Memory;
import hardware.Operator;
import hardware.Word;

public class Mul extends AritInstruction{

	protected Mul(Operator op1, Operator op2, Address address) {
		super(op1, op2, address);
	}

	@Override
	protected Word operation(Memory memory) {
		return op1.getWord(memory).mul(op2.getWord(memory));
	}

	@Override
	protected String arit() {
		return "MUL ";
	}

}
