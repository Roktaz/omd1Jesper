package software;

import hardware.Address;
import hardware.Memory;
import hardware.Operator;
import hardware.ProgramCounter;

public class Copy implements Instruction {
	 private Operator op;
	 private Address address;
	
	public Copy(Operator op, Address address) {
		this.op = op;
		this.address = address;
	}

	@Override
	public void execute(ProgramCounter pc, Memory memory) {
		memory.set(op.getWord(memory), address);
		pc.update();
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder("CPY ");
		sb.append(op + " " + address);
		return sb.toString();
	}

}
