package software;

import hardware.Address;
import hardware.Memory;
import hardware.ProgramCounter;

public class Print implements Instruction {
	private Address address;

	public Print(Address address) {
		this.address = address;
	}
	@Override
	public void execute(ProgramCounter pc, Memory memory) {
		System.out.println(address.getWord(memory).getValue() + "\n");
		pc.update();
	}
	public String toString(){
		return "PRT " + address;
	}

}
