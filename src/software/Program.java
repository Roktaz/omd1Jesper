package software;

import java.util.ArrayList;

public abstract class Program extends ArrayList<Instruction>{

	public String toString(){
		StringBuilder sb = new StringBuilder();
		int k = 0;
		for(Instruction i:this){
			sb.append(k + " ");
			sb.append(i.toString() + "\n");
			k++;
		}
		return sb.toString();
	}
	
}
