package software;

import hardware.Memory;
import hardware.ProgramCounter;

public class Jump implements Instruction{
	private int index;
	
	public Jump(int index) {
		this.index = index;
	}

	@Override
	public void execute(ProgramCounter pc, Memory memory) {
		pc.setCounter(index);
	}
	
	public String toString(){
		return "JMP " + index;
	}

}
