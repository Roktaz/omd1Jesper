package software;

import hardware.Memory;
import hardware.Operator;
import hardware.ProgramCounter;

public class JumpEq implements Instruction {
	private Operator op1;
	private Operator op2;
	private int index;

	public JumpEq(int index, Operator op1, Operator op2) {
		this.index = index;
		this.op1 = op1;
		this.op2 = op2;
	}
	@Override
	public void execute(ProgramCounter pc, Memory memory) {
		if(op1.getWord(memory).getValue() == op2.getWord(memory).getValue()){
			new Jump(index).execute(pc, memory);
		} else {
			pc.update();
		}
		
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder("JEQ " + index + " ");
		sb.append(op1 + " ");
		sb.append(op2);
		return sb.toString();
	}
}
