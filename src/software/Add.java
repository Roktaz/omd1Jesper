package software;

import hardware.Address;
import hardware.Memory;
import hardware.Operator;
import hardware.Word;

public class Add extends AritInstruction{
	
	public Add(Operator op1, Operator op2, Address address) {
		super(op1, op2, address);
	}

	@Override
	protected Word operation(Memory memory) {
		return op1.getWord(memory).add(op2.getWord(memory));
	}
	
	public String arit(){
		return "ADD ";
	}
}
