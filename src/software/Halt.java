package software;

import hardware.Memory;
import hardware.ProgramCounter;

public class Halt implements Instruction {

	public Halt() {
		
	}
	@Override
	public void execute(ProgramCounter pc, Memory memory) {
		pc.halt();
	}
	
	public String toString(){
		return "HLT";
	}

}
