package software;

import hardware.Address;
import hardware.Memory;
import hardware.Operator;
import hardware.ProgramCounter;
import hardware.Word;

public abstract class AritInstruction implements Instruction {
	protected Operator op1;
	protected Operator op2;
	private Address address;
	
	
	protected AritInstruction(Operator op1, Operator op2, Address address) {
		this.op1 = op1;
		this.op2 = op2;
		this.address = address;
	}
	
	@Override
	public void execute(ProgramCounter pc, Memory memory) {
		Word w = operation(memory);
		memory.set(w, address);
		pc.update();
	}

	protected abstract Word operation(Memory memory);
	
	protected abstract String arit();
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(arit());
		sb.append(op1 + " ");
		sb.append(op2 + " ");
		sb.append(address);
		return sb.toString();
	}
}
